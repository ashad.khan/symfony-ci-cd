start: ## Build and start the containers
	docker-compose up -d --build

stop: ## Stop containers
	docker-compose down

install: ## Install web dependencies
	docker exec -it my_app_web composer install
	docker exec -it my_app_web bin/console doctrine:migrations:migrate -n --allow-no-migration
	docker exec -it my_app_web bin/console cache:clear --no-warmup

test: ## Execute tests
	docker exec -it my_app_web vendor/bin/phpunit

web: ## Connect to web container
	docker exec -it my_app_web bash

db: ## Connect to db container
	docker exec -it my_app_mysql bash

init: ## Init project
	make start
	make install