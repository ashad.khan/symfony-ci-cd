<?php

namespace App\Tests\Entity;

use App\Entity\Pokemon;
use PHPUnit\Framework\TestCase;

class PokemonTest extends TestCase
{
    public function testPokemonName(): void
    {
        $pokemon = new Pokemon();
        $pokemon->setName('Pikachu');
        $this->assertEquals('Pikachu', $pokemon->getName());
    }
}
