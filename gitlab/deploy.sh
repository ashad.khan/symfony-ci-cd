DOCKER_TAG=`md5sum ./.docker/web/Dockerfile | awk '{ print $1 }'`

docker create --name $CI_COMMIT_REF_SLUG docker.io/$DOCKER_HUB_USERNAME/$DOCKER_HUB_PROJECT:$DOCKER_TAG || exit;

docker cp "$PWD/." $CI_COMMIT_REF_SLUG:$DOCKER_HUB_PROJECT_SRC

docker commit $CI_COMMIT_REF_SLUG docker.io/$DOCKER_HUB_USERNAME/$DOCKER_HUB_PROJECT:$DOCKER_HUB_IMAGE_WEB_DEV

docker run -it $CI_COMMIT_REF_SLUG composer install --working-dir=$DOCKER_HUB_PROJECT_SRC

docker rm $CI_COMMIT_REF_SLUG

docker push docker.io/$DOCKER_HUB_USERNAME/$DOCKER_HUB_PROJECT:$DOCKER_HUB_IMAGE_WEB_DEV
