DOCKER_TAG=`md5sum ./.docker/web/Dockerfile | awk '{ print $1 }'`

docker pull docker.io/$DOCKER_HUB_USERNAME/$DOCKER_HUB_PROJECT:$DOCKER_TAG && exit;

docker build ./.docker/web -t docker.io/$DOCKER_HUB_USERNAME/$DOCKER_HUB_PROJECT:$DOCKER_TAG
docker push docker.io/$DOCKER_HUB_USERNAME/$DOCKER_HUB_PROJECT:$DOCKER_TAG