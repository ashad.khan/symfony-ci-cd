# Symfony CI CD

## Install project

```
cd <PROJECT>
make init
http://localhost:8000
```

## Gitlab CI/CD variables

Add you personal variables in Settings > CI/CD > Variables

```
DOCKER_HUB_PROJECT=<Your docker hub project name>
DOCKER_HUB_PROJECT_SRC=<Your docker hub project path>
DOCKER_HUB_IMAGE_WEB_DEV=<Your docker hub dev image name>
DOCKER_HUB_USERNAME=<Your docker hub username>
DOCKER_HUB_TOKEN=<Your docker hub token or password>
```

The docker registry uri is like: `docker.io/$DOCKER_HUB_USERNAME/$DOCKER_HUB_PROJECT:<your tag>`

## Update docker-compose.yml file

Update the `webdev` image by your personal dev image in docker hub.
If you want, you can also replace the `web` image by yours.

## Tests and Deploy with CI/CD

First create a new `develop` branch and update for example your `DefaultController` response and commit/push your changes.
```
git checkout -b develop
<Make your changes>
git add .
git commit -m "Changes"
git push
```

After that, make sure your tests passed successfully in Gitlab pipelines and **execute manually the deployment**

Rebuild your project and check if your dev image is updated:
```
make stop
make start
http://localhost:8001
```

You should have to be a new builded image named with a random token in your docker hub to keep a version of the image.